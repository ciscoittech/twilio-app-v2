const Users = require('../models/users');
const cron = require("node-cron");
const globalConfig = require('../config/global');

const express = require('express');
const app = express();

module.exports.addUsers = function(newUsersData, res) {

   Users.addUsers(newUsersData, (err, results) => {
        console.log('added');

        res.json({
            result: true,
            msg: "add users successfully"
        })
   });
   
}

module.exports.updateUsers = function(upData, res) {

    Users.updateUsers(upData, (err, results) => {
         console.log('updated');
 
         res.json({
             result: true,
             data: results,
             msg: "updated users successfully"
         })
    });
    
 }

module.exports.checkUsers = function(UsersData, res) {

    Users.checkUsers(UsersData, (err, results) => {
        console.log('c res : ' + JSON.stringify(results));
         if(results.length != 0) {
            res.json({
                result: true,
                data: results[0],
                msg: "logined successfully"
            })
         } else {
            res.json({
                result: false,
                msg: "Invalide credentials"
            })
         }
    });
    
 }
