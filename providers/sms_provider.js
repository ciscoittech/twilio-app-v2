const Sid = 'AC0fdc94b6eef44d9124c2586b78dc58ac';
const authToken = 'ae212eb3e8f439cbb9572808f2d7abd1';
const clientTwilio = require('twilio')(Sid, authToken);

const cron = require("node-cron");

const Users = require('../models/users');
const Numbers = require('../models/numbers');

const hostname = 'http://103.19.16.62/';

function removeDuplicates(arr){
    let unique_array = []
    for(let i = 0;i < arr.length; i++){
        if(unique_array.indexOf(arr[i]) == -1){
            unique_array.push(arr[i])
        }
    }
    return unique_array
}

function sendAll(data, texts) {
    let num = data.pop();
    let total = data.length;

    console.log('numbers pop : ' + num);

    clientTwilio.messages
        .create({
            body: texts,
            to: num,
            from: '+13012347298'
        })
        .then(message => {
            console.log('SMS : ' + message.sid);
            if(total > 0) {
                sendAll(data, texts);
            }
            else {
                console.log('sent all');
            }
        }, err => {
            console.log('err : ' + err);
        })
        .done();

}

module.exports.sendSMS = function(param) {
        
    Numbers.loadSMSNumbersByUID(param, (err, nres) => {
        num_array = [];
        for(let i = 0; i < nres.length; i++) {
            num_array.push(nres[i].phone_number);
        }
        
        num_array = removeDuplicates(num_array);
        
        console.log('test num : ' + JSON.stringify(num_array));
                    
        if(num_array.length != 0) {
            sendAll(num_array, param.sms_text);
        }
                    
    });
}
