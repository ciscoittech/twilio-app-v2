const Numbers = require('../models/numbers');
const cron = require("node-cron");
const globalConfig = require('../config/global');

const express = require('express');
const app = express();

module.exports.addNumbers = function(number, res) {

    Numbers.addNumbers(number, (err, results) => {
        console.log('added');

        res.json({
            result: true,
            msg: "added number successfully"
        })
   });
   
}

module.exports.loadNumbers = function(number, res) {

    Numbers.loadNumbers(number, (err, results) => {
        console.log('loaded');

        res.json({
            result: true,
            data: results,
            msg: "loaded number successfully"
        })
   });
   
}

module.exports.removeNumbers = function(number, res) {

    Numbers.removeNumbers(number, (err, results) => {
        console.log('removed');

        res.json({
            result: true,
            msg: "removed number successfully"
        })
   });
   
}

