const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const TwilioProvider = require('../providers/twilio_provider');

router.post('/test', (req, res, next) => {
    TwilioProvider.doTest(req.body);
});

module.exports = router;