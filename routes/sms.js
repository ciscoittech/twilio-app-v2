const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const SMSProvider = require('../providers/sms_provider');

router.post('/send_sms', (req, res, next) => {
    SMSProvider.sendSMS(req.body);
});

module.exports = router;