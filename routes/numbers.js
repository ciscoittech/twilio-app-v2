const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Numbers = require('../models/numbers');
const NumbersProvider = require('../providers/numbers_provider');

router.post('/add', (req, res, next) => {
    NumbersProvider.addNumbers(req.body, res);
});

router.post('/load', (req, res, next) => {
    NumbersProvider.loadNumbers(req.body, res);
});

router.post('/remove', (req, res, next) => {
    NumbersProvider.removeNumbers(req.body, res);
});

module.exports = router;