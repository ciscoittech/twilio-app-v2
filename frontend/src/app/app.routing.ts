import { Routes } from '@angular/router';
import { AuthGuard } from './services/auth.guard';

import { LoginComponent }   from './components/login/login.component';
import { SignupComponent }   from './components/signup/signup.component';
import { HomeComponent }   from './components/home/home.component';
import { ProfileComponent }   from './components/profile/profile.component';
import { HelpComponent }   from './components/help/help.component';
import { VoiceCallComponent }   from './components/voice-call/voice-call.component';
import { SmsComponent }   from './components/sms/sms.component';
import { SettingsComponent }   from './components/settings/settings.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'help',
        component: HelpComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'voice-call',
        component: VoiceCallComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'sms',
        component: SmsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'signup',
        component: SignupComponent
    },
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard]
    }
]
