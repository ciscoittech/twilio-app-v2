import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';
import { NguiMapModule} from '@ngui/map';

import { LoginComponent }   from './components/login/login.component';
import { SignupComponent }   from './components/signup/signup.component';
import { HomeComponent }   from './components/home/home.component';
import { ProfileComponent }   from './components/profile/profile.component';
import { HelpComponent }   from './components/help/help.component';
import { VoiceCallComponent }   from './components/voice-call/voice-call.component';
import { SmsComponent }   from './components/sms/sms.component';
import { SettingsComponent }   from './components/settings/settings.component';

import { NumberService, AuthService, AuthGuard, NotificationService } from './services/index';

@NgModule({
  declarations: [
    AppComponent,

    LoginComponent,
    SignupComponent,
    HomeComponent,
    ProfileComponent,
    HelpComponent,
    VoiceCallComponent,
    SmsComponent,
    SettingsComponent
  ],
  imports: [
    HttpModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    SidebarModule,
    NavbarModule,
    FooterModule,
    FixedPluginModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'})

  ],
  providers: [
    NumberService, 
    AuthService, 
    AuthGuard,
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
