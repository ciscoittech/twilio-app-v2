import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { environment } from "../../environments/environment";

@Injectable()
export class NumberService {
    constructor(
        private http: Http) {
    }

    loadNumbers(data: any): Observable<any> {

        let headers = new Headers({ });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(environment.base_url + 'numbers/load', data, options)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    removeNumbers(data: any): Observable<any> {

        let headers = new Headers({ });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(environment.base_url + 'numbers/remove', data, options)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    addNumbers(data: any): Observable<any> {

        let headers = new Headers({ });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(environment.base_url + 'numbers/add', data, options)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    testNumbers(data: any): Observable<any> {

        let headers = new Headers({ });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(environment.base_url + 'twilio/test', data, options)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    sendSMS(data: any): Observable<any> {

        let headers = new Headers({ });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(environment.base_url + 'sms/send_sms', data, options)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }
}
