import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { environment } from "../../environments/environment";

@Injectable()
export class AuthService {
    constructor(
        private http: Http) {
    }

    register(data) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(environment.base_url + 'users/signup', data, {headers: headers})
            .map(res => res.json());
    }

    login(data) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(environment.base_url + 'users/login', data, {headers: headers})
            .map(res => res.json());
    }

    update(data) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(environment.base_url + 'users/update', data, {headers: headers})
            .map(res => res.json());
    }

    storeUserData(data) {
        localStorage.setItem('tuser', JSON.stringify(data));
    }

    loggedIn() {
        let user = localStorage.getItem('tuser');
        if(user) {
            return true;
        } else {
            return false;
        }
    }

    logOut() {
        localStorage.removeItem('tuser');
    }
}
