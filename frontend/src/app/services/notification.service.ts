import { Injectable } from '@angular/core';

declare var $:any;

@Injectable()
export class NotificationService {
    constructor() {
    }

    showNotification(msg, color){
        var type = ['','info','success','warning','danger'];

    	$.notify({
        	icon: "ti-bell",
        	message: msg
        },{
            type: type[color],
            timer: 4000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
    }

}
