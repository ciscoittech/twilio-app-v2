import { Component, OnInit } from '@angular/core';
import { NumberService, NotificationService, AuthService } from '../../services';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'sms-cmp',
    moduleId: module.id,
    templateUrl: 'sms.component.html',
    styleUrls: ['sms.component.css']
})

export class SmsComponent implements OnInit{
    public numbers: TableData;
    public sms_text = '';

    model: any = {};
    smsmodel: any = {};

    constructor(private numberService: NumberService,
                private notificationService: NotificationService,
                private authService: AuthService) {

    }

    ngOnInit(){
        this.numbers = {
            headerRow: [ 'Phone Number', 'Edit'],
            dataRows: []
        };
        this.loadNumbers();
        this.loadSMStext();
    }

    loadNumbers() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        this.numberService.loadNumbers({uid: userData.data._id, category: 1}).subscribe(data => {
            this.numbers.dataRows = data.data;
        }, err => {
            console.log(err);
        });
    }

    loadSMStext() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        if(userData.data.sms_text)
            this.sms_text = userData.data.sms_text;
    }

    removeNumber(id) {
        this.numberService.removeNumbers({_id: id}).subscribe(data => {
            this.loadNumbers();
        }, err => {
            console.log(err);
        });
    }

    doAddSMS() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        this.authService.update({_id: userData.data._id, sms_text: this.smsmodel.sms_texts}).subscribe(data => {
            console.log('usms : ' + this.smsmodel.sms_texts);
            userData.data.sms_text = this.smsmodel.sms_texts;
            localStorage.setItem('tuser', JSON.stringify(userData));
            this.loadSMStext();
        }, err => {
            console.log(err);
        });
    }

    doAdd() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        this.numberService.addNumbers({uid: userData.data._id, phone_number: this.model.phone_number, category: 1}).subscribe(data => {
            this.loadNumbers();
        }, err => {
            console.log(err);
        });
    }

    doSendSMS() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        this.notificationService.showNotification(
            "<b>Success</b>  <p>sent sms successfully</p>",
            2
        );
        this.numberService.sendSMS({uid: userData.data._id, sms_text: this.sms_text}).subscribe(data => {
        }, err => {
            console.log(err);
        });
    }
}
