import { Component, OnInit } from '@angular/core';
import { NotificationService, AuthService } from '../../services';

@Component({
    selector: 'settings-cmp',
    moduleId: module.id,
    templateUrl: 'settings.component.html',
    styleUrls: ['settings.component.css']
})

export class SettingsComponent implements OnInit{

    status: any = {voice_call: true};
    constructor(private notificationService: NotificationService,
                private authService: AuthService) {
        
    }

    ngOnInit(){
        this.loadStatus();
    }

    loadStatus() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        this.status.voice_call = userData.data.status;
    }

    doChange() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        console.log(this.status.voice_call);
        this.authService.update({_id: userData.data._id, status: this.status.voice_call}).subscribe(data => {
            userData.data.status = this.status.voice_call;
            localStorage.setItem('tuser', JSON.stringify(userData));
        }, err => {
            console.log(err);
        });
    }

}
