import { Component, OnInit } from '@angular/core';
import { AuthService, NotificationService } from '../../services/index';
import { Router } from '@angular/router';

@Component({
    selector: 'signup-cmp',
    moduleId: module.id,
    templateUrl: 'signup.component.html',
    styleUrls: ['signup.component.css']
})

export class SignupComponent implements OnInit{
    model: any = {};
    
    constructor(private authService: AuthService,
                private router: Router,
                private notificationService: NotificationService) {

    }

    ngOnInit(){
    }

    doSignup() {
        this.authService.register(this.model).subscribe(data => {
            if(data.result == true) {
                this.notificationService.showNotification(
                    "<b>Success</b>  <p>you're registered successfully.</p>",
                    2
                );
                this.router.navigate(['/login']);
            }
        }, err => {
            console.log(err);
        });
    }
}
