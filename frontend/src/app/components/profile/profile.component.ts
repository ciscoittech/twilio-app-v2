import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'profile-cmp',
    moduleId: module.id,
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.css']
})

export class ProfileComponent implements OnInit{
    userData: any = {};
    
    ngOnInit(){
        this.loadUserData();
    }

    loadUserData() {
        this.userData = JSON.parse(localStorage.getItem('tuser'));
    }
}
