import { Component, OnInit } from '@angular/core';
import { NumberService, NotificationService } from '../../services';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'voice-call-cmp',
    moduleId: module.id,
    templateUrl: 'voice-call.component.html',
    styleUrls: ['voice-call.component.css']
})

export class VoiceCallComponent implements OnInit{
    public numbers: TableData;
    model: any = {};

    constructor(private numberService: NumberService,
                private notificationService: NotificationService) {

    }

    ngOnInit(){
        this.numbers = {
            headerRow: [ 'Phone Number', 'Edit'],
            dataRows: []
        };
        this.loadNumbers();
    }

    loadNumbers() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        this.numberService.loadNumbers({uid: userData.data._id, category: 0}).subscribe(data => {
            this.numbers.dataRows = data.data;
        }, err => {
            console.log(err);
        });
    }

    removeNumber(id) {
        this.numberService.removeNumbers({_id: id}).subscribe(data => {
            this.loadNumbers();
        }, err => {
            console.log(err);
        });
    }

    doAdd() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        this.numberService.addNumbers({uid: userData.data._id, phone_number: this.model.phone_number, category: 0}).subscribe(data => {
            this.loadNumbers();
        }, err => {
            console.log(err);
        });
    }

    doTest() {
        let userData = JSON.parse(localStorage.getItem('tuser'));
        this.notificationService.showNotification(
            "<b>Success</b>  <p>testing...</p>",
            2
        );
        this.numberService.testNumbers({uid: userData.data._id}).subscribe(data => {
        }, err => {
            console.log(err);
        });
    }
}
