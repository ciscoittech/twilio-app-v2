import { Component, OnInit } from '@angular/core';
import { AuthService, NotificationService } from '../../services/index';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'login-cmp',
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})

export class LoginComponent implements OnInit{
    model: any = {};
    
    constructor(private authService: AuthService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private notificationService: NotificationService) {

    }

    ngOnInit(){
    }

    doLogin() {
        this.authService.login(this.model).subscribe(data => {
            if(data.result == true) {
                this.notificationService.showNotification(
                    "<b>Success</b>  <p>you're logined successfully.</p>",
                    2
                );
                this.authService.storeUserData(data);
                this.router.navigate(['/profile']);
            } else {
                this.notificationService.showNotification(
                    "<b>Failed</b>  <p>your credentials is invalid</p>",
                    4
                );
            }
        }, err => {
            console.log(err);
        });
    }
}
