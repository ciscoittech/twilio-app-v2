import { Component } from '@angular/core';
import { AuthService } from "app/services/auth.service";
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent{

    url = '';

    constructor(
        private authService: AuthService,
        private router: Router,
        private activatedRoute: ActivatedRoute) {
        
        this.router.events
        .filter((event) => event instanceof NavigationEnd)
        .map(() => this.activatedRoute)
        .map((route) => {
            while (route.firstChild) route = route.firstChild;
            return route;
        })
        .filter((route) => route.outlet === 'primary')
        .mergeMap((route) => route.data)
        .subscribe((evt) => {
            
            console.log('changed : ' + this.router.url);
            this.url = this.router.url;

            /*$('html,body').animate({ scrollTop: $('.tophere').offset().top - 100 }, '1');*/
        });
        
    }

    isHome() {
        if(this.url == '/home') {
            return true;
        } else {
            return false;
        }
    }

    isLoggedIn() {
        return this.authService.loggedIn();
    }
}
