import { Component, OnInit } from '@angular/core';
import { AuthService } from "../services/auth.service";
import { Router, ActivatedRoute } from '@angular/router';

declare var $:any;

export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    { path: 'profile', title: 'Profile',  icon:'ti-user', class: '' },
    { path: 'help', title: 'Help',  icon:'ti-help-alt', class: '' },
    { path: 'voice-call', title: 'Automated Voice Call',  icon:'ti-package', class: '' },
    { path: 'sms', title: 'Automated SMS',  icon:'ti-package', class: '' },
    { path: 'settings', title: 'Settings',  icon:'ti-settings', class: '' }
];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
    styleUrls: ['sidebar.component.css']
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];

    constructor(
        private authService: AuthService,
        private router: Router,
        private activatedRoute: ActivatedRoute) {
    }

    isLoggedIn() {
        return this.authService.loggedIn();
    }

    doLogOut() {
        this.authService.logOut();
        this.router.navigate(['/']);
    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    isNotMobileMenu(){
        if($(window).width() > 991){
            return false;
        }
        return true;
    }

}
